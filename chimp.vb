Rem Attribute VBA_ModuleType=VBAModule
Option VBASupport 1
Sub MailChimpMacro()
'
' MailChimpMacro Macro
'
' Keyboard Shortcut: Ctrl+Shift+M
'
    With ActiveSheet.QueryTables.Add(Connection:="TEXT;C:\csv\studentim.CSV", _
        Destination:=Range("$A$1"))
        .Name = "studentim"
        .FieldNames = True
        .RowNumbers = False
        .FillAdjacentFormulas = False
        .PreserveFormatting = True
        .RefreshOnFileOpen = False
        .RefreshStyle = xlInsertDeleteCells
        .SavePassword = False
        .SaveData = True
        .AdjustColumnWidth = True
        .RefreshPeriod = 0
        .TextFilePromptOnRefresh = False
        .TextFilePlatform = 38598
        .TextFileStartRow = 1
        .TextFileParseType = xlDelimited
        .TextFileTextQualifier = xlTextQualifierDoubleQuote
        .TextFileConsecutiveDelimiter = False
        .TextFileTabDelimiter = True
        .TextFileSemicolonDelimiter = False
        .TextFileCommaDelimiter = False
        .TextFileSpaceDelimiter = False
        .TextFileColumnDataTypes = Array(1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1)
        .TextFileTrailingMinusNumbers = True
        .Refresh BackgroundQuery:=False
    End With
    Sheets("Sheet1").Select
    Rows("1:1").Select
    Selection.Insert Shift:=xlDown, CopyOrigin:=xlFormatFromLeftOrAbove
    Range("A1").Select
    ActiveCell.FormulaR1C1 = "Lname"
    Range("B1").Select
    ActiveCell.FormulaR1C1 = "Fname"
    Range("C1").Select
    ActiveCell.FormulaR1C1 = "ID"
    Range("D1").Select
    ActiveCell.FormulaR1C1 = "email"
    Range("E1").Select
    ActiveCell.FormulaR1C1 = "Telephone"
    Range("F1").Select
    ActiveCell.FormulaR1C1 = "Cellular"
    Range("G1").Select
    ActiveCell.FormulaR1C1 = "Adress"
    Range("H1").Select
    ActiveCell.FormulaR1C1 = "ZIP"
    Range("I1").Select
    ActiveCell.FormulaR1C1 = "campus"
    Range("J1").Select
    ActiveCell.FormulaR1C1 = "faculty"
    Range("K1").Select
    ActiveCell.FormulaR1C1 = "degree"
    Range("L1").Select
    ActiveCell.FormulaR1C1 = "year"
    Range("M1").Select
    ActiveCell.FormulaR1C1 = "main"
    Range("N1").Select
    ActiveCell.FormulaR1C1 = "secondary"
    Range("O1").Select
    ActiveCell.FormulaR1C1 = "Age"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "Revaha"
    Range("P1").Select
    ActiveCell.FormulaR1C1 = "sex"
    Range("S1").Select
    ActiveCell.FormulaR1C1 = "revaha"
    Columns("Q:R").Select
    Range("R1").Activate
    Selection.Delete Shift:=xlToLeft
    Columns("O:P").Select
    Range("P1").Activate
    Selection.Delete Shift:=xlToLeft
    Columns("E:H").Select
    Range("H1").Activate
    Selection.Delete Shift:=xlToLeft
    ActiveWindow.ScrollColumn = 3
    ActiveWindow.ScrollColumn = 2
    ActiveWindow.ScrollColumn = 1
    Cells.Select
    Selection.ColumnWidth = 4
    Selection.ColumnWidth = 13.14
    Columns("C:C").Select
    Selection.Delete Shift:=xlToLeft
    Cells.Select
    Sheets("Sheet1").Select
End Sub
